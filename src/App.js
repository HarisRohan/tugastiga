import React , {useState} from 'react';
import {ScrollView, View, TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import {Action, Caption, Header, Post, Story, Suggestion, User} from  './components';

const Apps = () => {
    return (
      <View>
      <Header/>
        <ScrollView>
          <Story/>
          <User/>
          <Post/>
          <Action/>
          <Caption/>
          <Suggestion/>
          <User/>
          <Post/>
          <Action/>
          <Caption/>
          <User/>
          <Post/>
          <Action/>
          <Caption/>
        </ScrollView>
      </View>
    );
};

export default Apps;