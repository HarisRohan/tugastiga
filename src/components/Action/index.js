import React from 'react';
import {TouchableOpacity, View, Styles, StyleSheet, Image} from 'react-native';
import Heart from '../../assets/Notification.png';
import Comment from '../../assets/Comment.png';
import Message from '../../assets/Message.png';
import Save from '../../assets/Save.png';

const Action = () => {
    return (
        <View style={style.container}>
            <View style={style.mainAction}>
                <TouchableOpacity>
                    <Image source={Heart} style={{resizeMode : 'center', width : 30, height : 30}}/>
                </TouchableOpacity>
                <Image source={Comment} style={{resizeMode : 'center', width : 25, height : 30}}/>
                <Image source={Message} style={{resizeMode : 'center', width : 25, height : 30}}/>
            </View>
            <Image source={Save} style={{resizeMode : 'center', width : 20, height : 30, right : 20, position : 'absolute',}}/>
        </View>
    );
};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'center',
        height : 50,
        backgroundColor : 'white',
    },
    mainAction: {
        display : 'flex',
        flexDirection : 'row',
        width : 150,
        justifyContent : 'space-evenly',
        position : 'absolute',
        left : 0,
    },
});

export default Action;