import React from 'react';
import {ScrollView, View, TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import Profile from '../../assets/Profile.png';

const Suggestion = () => {
    return (
        <ScrollView horizontal={true}>
            <View style={style.container}>
                <View style={style.profile}>
                    <Image source={Profile} style={{resizeMode : 'center', height : 150, width : 150,}}/>
                    <Text style={{fontSize : 15, fontWeight : 'bold', textAlign : 'center', paddingTop : 10,}}>User</Text>
                    <Text style={{fontSize : 12, padding : -10, color : 'grey'}}>Followed by username </Text>
                    <TouchableOpacity style={style.follow} onPress={null}>
                        <Text style={{fontSize : 15, fontWeight : 'bold', color : 'white', textAlign : 'center', padding : 3,}}>Follow</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={style.container}>
                <View style={style.profile}>
                    <Image source={Profile} style={{resizeMode : 'center', height : 150, width : 150,}}/>
                    <Text style={{fontSize : 15, fontWeight : 'bold', textAlign : 'center', paddingTop : 10,}}>User</Text>
                    <Text style={{fontSize : 12, padding : -10, color : 'grey'}}>Follows you</Text>
                    <TouchableOpacity style={style.follow} onPress={null}>
                        <Text style={{fontSize : 15, fontWeight : 'bold', color : 'white', textAlign : 'center', padding : 3,}}>Follow back</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={style.container}>
                <View style={style.profile}>
                    <Image source={Profile} style={{resizeMode : 'center', height : 150, width : 150,}}/>
                    <Text style={{fontSize : 15, fontWeight : 'bold', textAlign : 'center', paddingTop : 10,}}>User</Text>
                    <Text style={{fontSize : 12, padding : -10, color : 'grey'}}>Followed by username </Text>
                    <TouchableOpacity style={style.follow} onPress={null}>
                        <Text style={{fontSize : 15, fontWeight : 'bold', color : 'white', textAlign : 'center', padding : 3,}}>Follow</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={style.container}>
                <View style={style.profile}>
                    <Image source={Profile} style={{resizeMode : 'center', height : 150, width : 150,}}/>
                    <Text style={{fontSize : 15, fontWeight : 'bold', textAlign : 'center', paddingTop : 10,}}>User</Text>
                    <Text style={{fontSize : 12, padding : -10, color : 'grey'}}>Follows you</Text>
                    <TouchableOpacity style={style.follow} onPress={null}>
                        <Text style={{fontSize : 15, fontWeight : 'bold', color : 'white', textAlign : 'center', padding : 3,}}>Follow back</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    );

};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        flex : 3,
        borderWidth : 0.5,
        borderRadius : 10,
        borderColor : 'grey',
        flexDirection : 'column',
        alignItems : 'center',
        // justifyContent : 'center',
        height : 300,
        padding : 30,
        marginBottom : 50,
        margin : 5,
    },
    profile : {
        display : 'flex',
        flex : 3,
        alignItems : 'center',
        margin : 0,
    },
    follow : {
        alignItems : 'center',
        borderRadius : 3,
        width : 150,
        height : 25,
        marginTop : 20,
        backgroundColor : '#1da1f2',
    },
});

export default Suggestion;