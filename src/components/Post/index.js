import React from 'react';
import {View, ScrollView, Styles, Text, StyleSheet, Image} from 'react-native';
import Post from '../../assets/Post.jpg';

const Content = () => {
    return (
        <View style={style.container}>
            <Text style={{fontWeight : 'bold', fontSize : 20}}>Your post goes here</Text>
        </View>
    );

};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        alignItems : 'center',
        justifyContent : 'center',
        height : 400,
        backgroundColor : 'grey',
    },
});

export default Content;